## Themes

Package to add theme and asset support to Laravel 5

## API Documentation

API documentation can be found in the api directory.

### License

This package is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
